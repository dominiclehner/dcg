﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Registration : MonoBehaviour {

	public InputField username;
	public InputField password;
	public InputField passwordConfirm;

	EventSystem system;

	public GameObject Notification;
	public Text NotificationText;
	public GameObject NotificationButton;

	public GameObject loadAnimation;
	public Image loadImage;

	public GameObject enterButton;

	bool verified;

	void Start () {
		NotificationText.text = "Bitte warten...";
		Notification.SetActive (false);
		loadAnimation.SetActive (false);
		NotificationButton.SetActive (false);
		system = EventSystem.current;
		verified = false;
	}

	// Update is called once per frame
	void Update () {
		loadImage.transform.Rotate(0.0f, 0.0f, -1.0f);
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			
			if (next!= null) {
				
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield !=null) inputfield.OnPointerClick(new PointerEventData(system));
				
				system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
			}
		}
	}

	public void ClickedRegister () {
		Debug.Log (username.text);
		Debug.Log (password.text);
		Debug.Log (passwordConfirm.text);
		if (username.text != "" || password.text != "" || passwordConfirm.text != "") {
			Debug.Log("1");
			if (password.text == passwordConfirm.text && password.text != "" && passwordConfirm.text != "") {
				Notification.SetActive(true);
				NotificationButton.SetActive(false);
				loadAnimation.SetActive(true);
				StartCoroutine (SendData (username.text, password.text));
				Debug.Log("2");
			} else {
				username.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
				password.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
				passwordConfirm.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
				password.text = "";
				passwordConfirm.text = "";
				Debug.Log ("Registration data is invalid!");
			}
		} else {
			username.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
			password.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
			passwordConfirm.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
			Debug.Log ("Registration data is invalid!");
		}
	}

	public IEnumerator SendData(string username, string password) {
		string saveUrl = "http://digitalcardgame.bplaced.net/register_user.php";
		Debug.Log ("Connected to webserver!");
		Debug.Log (username);
		Debug.Log (password);
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("password", password);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		Debug.Log (www.text);
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
		} else {
			if (www.text != "False") {
				Debug.Log ("Registered as " + username + "!");
				verified = true;
				NotificationButton.SetActive(true);
				loadAnimation.SetActive(false);
				enterButton.GetComponentInChildren<Button>().interactable = false;
				NotificationText.text = "Erfolgreich Registriert! Wir wünschen dir noch viel Spass!";
				Notification.SetActive(true);
			} else if (www.text == "False") {
				NotificationButton.SetActive(true);
				loadAnimation.SetActive(false);
				enterButton.GetComponentInChildren<Button>().interactable = false;
				NotificationText.text = "Der Benutername ist bereits vergeben.";
				Notification.SetActive(true);
			}
			www.Dispose ();
		}
		username = "";
		password = "";
	}

	public void BackClicked() {
		StartCoroutine (LoadLoginScene());
	}
	
	public IEnumerator LoadLoginScene() {
		yield return new WaitForSeconds (0.3f);
		Application.LoadLevel ("Login");
	}

	public void ClickedOk() {
		if (verified) {
			Application.LoadLevel ("Login");
		} else {
			Notification.SetActive(false);
			enterButton.GetComponentInChildren<Button>().interactable = true;
		}
	}
}
