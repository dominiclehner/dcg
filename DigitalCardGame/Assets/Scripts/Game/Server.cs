﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Server : MonoBehaviour {

	public static List<NetworkPlayer> waiters;
	public GameObject diener;

	void Awake() {
	}

	// Use this for initialization
	void Start () {
		waiters = new List<NetworkPlayer> ();
	}

	public void StartServer () {
		Network.InitializeServer (1, 1521, true);
		MasterServer.RegisterHost(Parameter.registeredGameName, Parameter.Username+"DCG");
		Debug.Log ("Created Server " + Parameter.registeredGameName);
	}
	
	// Update is called once per frame
	void Update () {
		if (waiters.Count >= 1) {
			networkView.RPC("LoadGame",RPCMode.All);
		}
	}

	[RPC]
	public void AddPlayer (NetworkMessageInfo info){
		if (true) {
			waiters.Add (info.sender);
			Debug.Log ("Spieler: " + waiters.Count);
		}
	}
}
