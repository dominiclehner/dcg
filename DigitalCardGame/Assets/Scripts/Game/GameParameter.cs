﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameParameter : MonoBehaviour {

	public static List<Card> playableCards;

	public static GameObject[] playableCardsGameObjects;

	public static GameObject Diener;
	public static GameObject Distraction;
	public static List<GameObject> allServants;
	public static List<GameObject> allServantsOpponent;

	public static int energyCounter;

	public static NetworkPlayer player1;
	public static NetworkPlayer player2;

	public static GameObject myServant = null;
	public static GameObject opponentObject = null;
	public static GameObject specialAttack = null;

	public static GameObject opponent;
	public static GameObject character;

	public static GameObject characterSpecialAttack;
	public static GameObject opponentSpecialAttack;

	public static bool myTurn;

	public static bool bothFinishedCardSelection;

	public static int roundCounter;

	public static float timer;

	public static bool gameFinished;

	public static int characterID;

	public static Texture[] playedServantsDesign;
}
