﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour {
	
	public Vector3 originPosition;
	public Quaternion originRotation;
	public float shakeDecay;
	public float shakeIntensity;
	
	void OnGUI (){
		if (GUI.Button (new Rect (20,40,80,20), "Shake")){
			Shake ();
		}
	}
	
	void Update (){
		if (shakeIntensity > 0){
			transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
			transform.rotation = new Quaternion(
				originRotation.x + Random.Range (-shakeIntensity,shakeIntensity) * .1f,
				originRotation.y + Random.Range (-shakeIntensity,shakeIntensity) * .1f,
				originRotation.z + Random.Range (-shakeIntensity,shakeIntensity) * .1f,
				originRotation.w + Random.Range (-shakeIntensity,shakeIntensity) * .1f);
			shakeIntensity -= shakeDecay;
		}
	}
	
	public void Shake(){
		originPosition = transform.position;
		originRotation = transform.rotation;
		shakeIntensity = .1f;
		shakeDecay = 0.002f;
	}
}
