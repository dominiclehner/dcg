using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class HighlightPlayableCard : MonoBehaviour {
	
	private Vector3 originalPosition;
	private Quaternion originalRotation;
	public Color initialColor;
	public GameObject card;
	
	public GameObject Diener;
	public GameObject Distraction;
	
	public Canvas canvas;
	
	private Vector3 screenPoint;
	private Vector3 offset;
	public static float counter = 0;
	public static int counter1 = 0;
	
	public Vector3 old_pos;
	
	public bool check;
	
	public GameObject CardToDisplayPanel;
	public GameObject CardToDisplay;
	
	public Sprite basicSprite;
	public Sprite specialSprite1;
	public Sprite specialSprite2;
	public Sprite specialSprite3;
	public Sprite specialSprite4;
	
	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite4;
	
	bool isOverCard;
	
	public List<string> chatHistory = new List<string>();
	
	public Texture[] attackPoints;
	public Texture[] lifePoints;
	
	public Texture[] playedServantsDesign; // Basic = 0
	
	public GameObject opponentsCard;
	
	public string initialSortingLayer;
	
	public Texture[] attackPointsKaPoom;
	public Texture[] lifePointsKaPoom;

	public GameObject attackServantsParticle;
	public GameObject attackServantsParticleOpponent;
	
	// Use this for initialization
	void Start () {
		GameParameter.Diener = Diener;
		GameParameter.Distraction = Distraction;
		GameParameter.playedServantsDesign = playedServantsDesign;
		chatHistory.Clear ();
		isOverCard = false;
		check = false;
		old_pos = transform.position;
		CardToDisplayPanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnMouseDown()
	{
		isOverCard = true;
		CardToDisplayPanel.SetActive (true);
		check = true;
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		//canvas.transform.position += new Vector3 (0f, 10f, 0f);
	}
	
	void OnMouseDrag()
	{
		isOverCard = true;
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		transform.position = Camera.main.ScreenToWorldPoint(curScreenPoint);
		//gameObject.transform.position += new Vector3 (0, 0.5f, 0.3f);
	}
	
	void OnMouseEnter () {
		CardToDisplayPanel.SetActive (true);
		if (!isOverCard) {
			gameObject.transform.position += new Vector3 (0f, 1f, 30f);
			initialSortingLayer = gameObject.GetComponentInChildren<SpriteRenderer> ().sortingLayerName;
		}
		gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "9";
		gameObject.GetComponentInChildren<Canvas>().sortingLayerName = "10";
		FillCardToDisplay ();
	}
	
	void FillCardToDisplay() {
		Text[] compCard = CardToDisplay.GetComponentsInChildren<Text>();
		Text[] l = card.GetComponentsInChildren<Text> ();
		string[] splitText = l[5].text.Split(' ');
		if (splitText[0] == "Fügt" || splitText[0] == "Zieht" || splitText[0] == "Erhöht") {
			compCard [0].GetComponent<Text> ().text = "";
			compCard [1].GetComponent<Text> ().text = "";
		} else {
			compCard [0].GetComponent<Text> ().text = l[0].text;
			compCard [1].GetComponent<Text> ().text = l[1].text;
		}
		compCard[2].text = l[2].text;
		compCard[3].text = l[3].text;
		compCard[4].text = l[4].text;
		compCard[5].text = l[5].text;
		if (l[3].text == "0") {
			CardToDisplay.GetComponent<Image>().sprite = basicSprite;
		} else if (l[3].text == "1") {
			if (splitText[0] == "Erhöht") {
				CardToDisplay.GetComponent<Image>().sprite = spellSpecialSprite1;
			} else {
				CardToDisplay.GetComponent<Image>().sprite = specialSprite1;
			}
		} else if (l[3].text == "2") {
			if (splitText[0] == "Fügt") {
				CardToDisplay.GetComponent<Image>().sprite = spellSpecialSprite2;
			} else {
				CardToDisplay.GetComponent<Image>().sprite = specialSprite2;
			}
		} else if (l[3].text == "3") {
			if (splitText[0] == "Zieht") {
				CardToDisplay.GetComponent<Image>().sprite = spellSpecialSprite3;
			} else {
				CardToDisplay.GetComponent<Image>().sprite = specialSprite3;
			}
		} else if (l[3].text == "4") {
			if (splitText[0] == "Erhöht") {
				CardToDisplay.GetComponent<Image>().sprite = spellSpecialSprite4;
			} else {
				CardToDisplay.GetComponent<Image>().sprite = specialSprite4;
			}
		}
	}
	
	void OnMouseExit () {
		if (!isOverCard) {
			CardToDisplayPanel.SetActive (false);
			gameObject.transform.position = old_pos;
			gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = initialSortingLayer;
			gameObject.GetComponentInChildren<Canvas>().sortingLayerName = initialSortingLayer;
		}
	}
	
	void OnMouseUp() {
		isOverCard = false;
		string[] name = this.name.Split(';');
		if (GameParameter.myTurn) {
			int checkEnergy = GameParameter.energyCounter - System.Int32.Parse (card.GetComponentsInChildren<Text> () [2].text);
			if (name[1] == "0" && transform.position.z > 300 && transform.position.z < 560 && checkEnergy >= 0) {
				Text[] t = card.GetComponentsInChildren<Text> ();
				int chara = 0;
				for (int i = 0; i < t.Length; i++) {
					if (t [i].name == "character") {
						chara = System.Int32.Parse (t [i].text);
					}
				}
				GameParameter.energyCounter -= System.Int32.Parse (card.GetComponentsInChildren<Text> () [2].text);
				SetDiener (
					card.GetComponentsInChildren<Text> () [0].text,
					card.GetComponentsInChildren<Text> () [1].text,
					card.GetComponentsInChildren<Text> () [5].text, 
					chara);
				networkView.RPC ("SetDienerGegner", RPCMode.Others, new object[] {
					card.GetComponentsInChildren<Text> () [0].text,
					card.GetComponentsInChildren<Text> () [1].text,
					card.GetComponentsInChildren<Text> () [5].text,
					chara
				});
				isOverCard = true;
				//this.transform.position += new Vector3 (1500f, 213f, 0f);
				name = this.name.Split(';');
				this.name = name[0]+";1";
				//old_pos = new Vector3 (-158.5f, -200f, 413f);
				
				this.transform.position = old_pos;
				gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = initialSortingLayer;
				gameObject.GetComponentInChildren<Canvas>().sortingLayerName = initialSortingLayer;
				Component[] compCard = this.GetComponentsInChildren<Text>();
				List<Card> newPlayableCards = new List<Card>();
				foreach (var c in GameParameter.playableCards) {
					if ((""+c.Id) != compCard[4].GetComponent<Text>().text) {
						newPlayableCards.Add(c);
					}
				}
				GameParameter.playableCards = null;
				GameParameter.playableCards = newPlayableCards;
				FillPlayableCards();
			} else {
				
				gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = initialSortingLayer;
				gameObject.GetComponentInChildren<Canvas>().sortingLayerName = initialSortingLayer;
				gameObject.transform.position = old_pos;
			}
			check = false;
			CardToDisplayPanel.SetActive (false);
		} else {
			gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = initialSortingLayer;
			gameObject.GetComponentInChildren<Canvas>().sortingLayerName = initialSortingLayer;
			gameObject.transform.position = old_pos;
			CardToDisplayPanel.SetActive (false);
		}
	}
	
	public void FillPlayableCards() {
		int count = 0;
		foreach (var card in GameParameter.playableCards) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			string[] splitText = card.Text.Split(' ');
			if (splitText[0] == "Fügt" || splitText[0] == "Zieht" || splitText[0] == "Erhöht") {
				compCard [0].GetComponent<Text> ().text = "";
				compCard [1].GetComponent<Text> ().text = "";
			} else {
				compCard [0].GetComponent<Text> ().text = card.Leben.ToString();
				compCard [1].GetComponent<Text> ().text = card.Angriff.ToString();
			}
			compCard[2].GetComponent<Text>().text = card.Energy.ToString();
			compCard[3].GetComponent<Text>().text = card.Character.ToString();
			compCard[4].GetComponent<Text>().text = card.Id.ToString ();
			compCard[5].GetComponent<Text>().text = card.Text;
			if (card.Character == 0) {
				cardObject.GetComponent<SpriteRenderer>().sprite = basicSprite;
			} else if (card.Character == 1) {
				if (splitText[0] == "Erhöht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite1;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite1;
				}
			} else if (card.Character == 2) {
				if (splitText[0] == "Fügt") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite2;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite2;
				}
			} else if (card.Character == 3) {
				if (splitText[0] == "Zieht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite3;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite3;
				}
			} else if (card.Character == 4) {
				if (splitText[0] == "Erhöht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite4;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite4;
				}
			}
			cardObject.GetComponent<BoxCollider>().enabled = true;
			count++;
		}
		while (count < GameParameter.playableCardsGameObjects.Count()) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			//GameParameter.playableCardsGameObjects[count].SetActive(false);
			compCard[0].GetComponent<Text>().text = "";
			compCard[1].GetComponent<Text>().text = "";
			compCard[2].GetComponent<Text>().text = "";
			compCard[3].GetComponent<Text>().text = "";
			compCard[4].GetComponent<Text>().text = "";
			compCard[5].GetComponent<Text>().text = "";
			cardObject.GetComponent<SpriteRenderer>().sprite = null;
			cardObject.GetComponent<BoxCollider>().enabled = false;
			count++;
		}
		foreach (var c in GameParameter.playableCardsGameObjects) {
			string[] name = c.name.Split(';');
			c.name = name[0]+";0";
		}
		isOverCard = false;
	}
	
	public void SetDiener (string life, string attack, string text, int charId){
		GameObject servant = null;
		String[] splitText = text.Split (' ');
		if (splitText[0] == "Fügt" || splitText[0] == "Zieht" || splitText[0] == "Erhöht") {
			if (splitText[0] == "Fügt") {
				int damage = Int32.Parse(splitText[5]);
				StartCoroutine(DamageServants(damage, GameParameter.allServantsOpponent));
			} else if (splitText[0] == "Erhöht") {
				if (splitText[2] == "Angriff") {
					int increaseAttack = Int32.Parse(splitText[6]);
					IncreaseDemageServants(increaseAttack, GameParameter.allServants);
				} else if (splitText[2] == "Leben") {
					int increaseLife = Int32.Parse(splitText[6]);
					IncreaseLifeServants(increaseLife, GameParameter.allServants);
				}
			} else if (splitText[0] == "Zieht") {
				int decreaseAttack = Int32.Parse(splitText[5]);
				DecreaseAttackServants(decreaseAttack, GameParameter.allServantsOpponent);
			}
		} else {
			if (splitText[0] == "Ablenkung") {
				servant = (GameObject)Instantiate(Distraction,new Vector3(0f,0f,0f),Quaternion.Euler(0,0,0));
				servant.name = "Diener;"+life+";"+attack+";1";
			} if (splitText[0] == "Bereitschaft") {
				servant = (GameObject)Instantiate(Diener,new Vector3(0f,0f,0f),Quaternion.Euler(0,0,0));
				servant.name = "Diener;"+life+";"+attack+";0";
			}else {
				servant = (GameObject)Instantiate(Diener,new Vector3(0f,0f,0f),Quaternion.Euler(0,0,0));
				servant.name = "Diener;"+life+";"+attack+";1";
			}
			servant.AddComponent<BoxCollider>();
			servant.transform.localScale = new Vector3(100,100,100);
			servant.transform.Rotate(-90f,-180f,0);
			
			GameObject objCharacterDesign = servant.transform.Find("Main").gameObject;
			objCharacterDesign.renderer.materials [1].mainTexture = playedServantsDesign[charId];
			GameObject objAttack = servant.transform.Find("Attack").gameObject;
			objAttack.renderer.materials [0].mainTexture = lifePoints[Int32.Parse(attack)];
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = attackPoints[Int32.Parse(life)];
			
			GameParameter.allServants.Add (servant);
			
			setDienerPosition ();
			ClickToAttack.colorDiener();
			if (splitText[0] != "Bereitschaft") {
				StartCoroutine(PlayCreateServantParticleSystem(servant, objCharacterDesign, objAttack, objLife));
			}
		}
	}
	
	[RPC]
	public void SetDienerGegner (string life, string attack, string text, int charId){
		GameObject servant = null;
		String[] splitText = text.Split (' ');
		if (splitText [0] != "Fügt" && splitText [0] != "Zieht" && splitText [0] != "Erhöht") {
			if (text == "Ablenkung") {
				servant = (GameObject)Instantiate (Distraction, new Vector3 (0f, 0f, 0f), Quaternion.Euler (0, 0, 0));
				servant.name = "DienerGegner;" + life + ";" + attack + ";1";
			} else {
				servant = (GameObject)Instantiate (Diener, new Vector3 (0f, 0f, 0f), Quaternion.Euler (0, 0, 0));
				servant.name = "DienerGegner;" + life + ";" + attack + ";0";
			}
			servant.AddComponent<BoxCollider> ();
			servant.transform.localScale = new Vector3 (100, 100, 100);
			servant.transform.Rotate (-90f, -180f, 0);
			servant.AddComponent<Servant> ();
			
			GameObject objCharacterDesign = servant.transform.Find ("Main").gameObject;
			objCharacterDesign.renderer.materials [1].mainTexture = playedServantsDesign [charId];
			GameObject objAttack = servant.transform.Find ("Attack").gameObject;
			objAttack.renderer.materials [0].mainTexture = lifePoints [Int32.Parse (attack)];
			GameObject objLife = servant.transform.Find ("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = attackPoints [Int32.Parse (life)];
			
			GameParameter.allServantsOpponent.Add (servant);
			
			setGegnerDienerPosition ();
			if (text != "Bereitschaft") {
				StartCoroutine(PlayCreateServantParticleSystem(servant, objCharacterDesign, objAttack, objLife));
				Debug.Log("Bereitschaft keine");
			} else {
				servant.transform.Find("Zzz").gameObject.SetActive(false);
			}
		} 
	}
	
	IEnumerator PlayCreateServantParticleSystem(GameObject servant, GameObject x, GameObject y, GameObject z) {
		x.SetActive (false);
		y.SetActive (false);
		z.SetActive (false);
		servant.transform.Find("Zzz").gameObject.SetActive(false);
		GameObject psObject = servant.transform.Find("CreateServant").gameObject;
		GameObject psObject2 = servant.transform.Find("Birth").gameObject;
		psObject.particleSystem.Emit(1);
		psObject2.particleSystem.Emit(1);
		yield return new WaitForSeconds (1f);
		x.SetActive (true);
		y.SetActive (true);
		z.SetActive (true);
		servant.transform.Find("Zzz").gameObject.SetActive(true);
	}
	
	//Leben meiner Diener erhöhen
	public void IncreaseLifeServants(int increaseLife, List<GameObject> servants) {
		int initialIncreaseLife = increaseLife;
		foreach (var servant in servants) {
			string[] name = servant.name.Split(';');
			if (Int32.Parse(name[1])+increaseLife > 10) {
				increaseLife = (10 - Int32.Parse(name[1]));	
			}
			networkView.RPC ("IncreaseServantLife", RPCMode.Others, new object[] {
				servant.name,
				(Int32.Parse(name[1])+increaseLife),
				increaseLife
			});
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = attackPoints[Int32.Parse(name[1])+increaseLife];
			GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
			Debug.Log (attackPointsKaPoom.Count());
			Debug.Log(increaseLife);
			psObject.particleSystem.renderer.material.mainTexture = lifePointsKaPoom[increaseLife];
			psObject.particleSystem.Emit(1);
			servant.name = "Diener" + ";" + (Int32.Parse(name[1])+increaseLife) + ";" + name[2] + ";" + name[3];
			increaseLife = initialIncreaseLife;
		}
	}
	
	/// <summary>
	/// Increases the life of my servants
	/// for opponent's view
	/// </summary>
	/// <param name="who">Who.</param>
	/// <param name="newLife">New life.</param>
	[RPC]
	public void IncreaseServantLife(string who, int newLife,int increase) {
		string[] name = who.Split (';');
		GameObject servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";1");
		if (servant == null) {
			servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";0");
		}
		string[] name1 = servant.name.Split(';');
		GameObject objLife = servant.transform.Find("Life").gameObject;
		objLife.renderer.materials [0].mainTexture = attackPoints[newLife];
		GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
		Debug.Log (attackPointsKaPoom.Count());
		Debug.Log(increase);
		psObject.particleSystem.renderer.material.mainTexture = lifePointsKaPoom[increase];
		psObject.particleSystem.Emit(1);
		servant.name = "DienerGegner" + ";" + newLife + ";" + name1[2] + ";" + name1[3];
	}
	
	//Angriff meiner Diener erhöhen
	public void IncreaseDemageServants(int increaseAttack, List<GameObject> servants) {
		int initialIncreaseAttack = increaseAttack;
		foreach (var servant in servants) {
			string[] name = servant.name.Split(';');
			if (Int32.Parse(name[2])+increaseAttack > 10) {
				increaseAttack = (10 - Int32.Parse(name[2]));	
			}
			networkView.RPC ("IncreaseServantAttack", RPCMode.Others, new object[] {
				servant.name,
				(Int32.Parse(name[2])+increaseAttack),
				increaseAttack
			});
			GameObject objLife = servant.transform.Find("Attack").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[Int32.Parse(name[2])+increaseAttack];
			GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
			Debug.Log (attackPointsKaPoom.Count());
			Debug.Log(increaseAttack);
			psObject.particleSystem.renderer.material.mainTexture = lifePointsKaPoom[increaseAttack];
			psObject.particleSystem.Emit(1);
			servant.name = "Diener" + ";" + name[1] + ";" + (Int32.Parse(name[2])+increaseAttack) + ";" + name[3];
			increaseAttack = initialIncreaseAttack;
		}
	}
	
	[RPC]
	public void IncreaseServantAttack(string who, int newAttack, int increaseAttack) {
		string[] name = who.Split (';');
		GameObject servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";1");
		if (servant == null) {
			servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";0");
		}
		string[] name1 = servant.name.Split(';');
		GameObject objLife = servant.transform.Find("Attack").gameObject;
		objLife.renderer.materials [0].mainTexture = lifePoints[newAttack];
		GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
		Debug.Log (attackPointsKaPoom.Count());
		Debug.Log(increaseAttack);
		psObject.particleSystem.renderer.material.mainTexture = lifePointsKaPoom[increaseAttack];
		psObject.particleSystem.Emit(1);
		servant.name = "DienerGegner" + ";" + name1[1] + ";" + newAttack + ";" + name1[3];
	}
	
	public void DecreaseAttackServants(int decreaseAttack, List<GameObject> servants) {
		int initialDecreaseAttack = decreaseAttack;
		foreach (var servant in servants) {
			string[] name = servant.name.Split(';');
			if (Int32.Parse(name[2]) - decreaseAttack < 0) {
				decreaseAttack = Int32.Parse(name[2]);
			}
			networkView.RPC ("DecreaseServantAttack", RPCMode.Others, new object[] { servant.name, (Int32.Parse(name[2])-decreaseAttack), decreaseAttack });
			GameObject objLife = servant.transform.Find("Attack").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[Int32.Parse(name[2])-decreaseAttack];
			GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
			Debug.Log (attackPointsKaPoom.Count());
			Debug.Log(decreaseAttack);
			psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[decreaseAttack];
			psObject.particleSystem.Emit(1);
			servant.name = "DienerGegner" + ";" + name[1] + ";" + (Int32.Parse(name[2])-decreaseAttack) + ";" + name[3];
			decreaseAttack = initialDecreaseAttack;
		}
	}
	
	[RPC]
	public void DecreaseServantAttack(string who, int newAttack, int decreaseAttack) {
		string[] name = who.Split (';');
		GameObject servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";1");
		if (servant == null) {
			servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";0");
		}
		string[] name1 = servant.name.Split(';');
		GameObject objLife = servant.transform.Find("Attack").gameObject;
		objLife.renderer.materials [0].mainTexture = lifePoints[newAttack];
		GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
		psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[decreaseAttack];
		psObject.particleSystem.Emit(1);
		servant.name = "Diener" + ";" + name1[1] + ";" + newAttack + ";" + name1[3];
	}
	
	//Leben gegner Diener abziehen
	public IEnumerator DamageServants(int damage, List<GameObject> servants) {
		StartCoroutine (PlayParticleSystemFireballs("me"));
		networkView.RPC ("LetTheFlamesBegin", RPCMode.Others, new object[] {});
		yield return new WaitForSeconds (5f);
		List<GameObject> deleteList = new List<GameObject> ();
		foreach (var servant in servants) {
			string[] name = servant.name.Split (';');
			if (Int32.Parse (name [1]) - damage <= 0) {
				networkView.RPC ("DeleteServant", RPCMode.Others, new object[] { 
					servant.name,
					damage
				});
				deleteList.Add (servant);
				//GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
				StartCoroutine (DeleteServantParticleSystem (servant, damage, GameParameter.allServantsOpponent));
			} else {
				networkView.RPC ("SetLife", RPCMode.Others, new object[] {
					servant.name,
					Int32.Parse (name [1]) - damage,
					damage
				});
				GameObject objLife = servant.transform.Find ("Life").gameObject;
				objLife.renderer.materials [0].mainTexture = attackPoints [Int32.Parse (name [1]) - damage];
				GameObject psObject = servant.transform.Find ("GotAttacked").gameObject;
				Debug.Log ("kapoom");
				psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom [damage];
				psObject.particleSystem.Emit (1);
				servant.name = "DienerGegner" + ";" + (Int32.Parse (name [1]) - damage) + ";" + name [2] + ";" + name [3];
			}
		}
		foreach (var servant in deleteList) {
			GameParameter.allServantsOpponent.Remove (servant);
		}
	}

	/// <summary>
	/// Emits the particle system
	/// after you/the opponent attacked with
	/// a spell, which damages all of your/his
	/// servants.
	/// </summary>
	/// <returns></returns>
	[RPC]
	public void LetTheFlamesBegin() {
		PlayParticleSystemFireballs ("opp");
	}

	public IEnumerator PlayParticleSystemFireballs(string x) {
		Debug.Log ("let the");
		GameObject psObject;
		if (x == "me") {
			psObject = attackServantsParticleOpponent;
		} else {
			psObject = attackServantsParticle;
		}
		psObject.particleSystem.Play ();
		yield return new WaitForSeconds (2f);
		psObject.particleSystem.Stop ();
	}

	[RPC]
	public void DeleteServant(string who, int substractAttackPoints) {
		string[] name = who.Split (';');
		if (name[0] == "Diener") {
			GameObject servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";1");
			if (servant == null) {
				servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";0");
			}
			StartCoroutine(DeleteServantParticleSystem(servant, substractAttackPoints, GameParameter.allServantsOpponent));
		} else {
			GameObject servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";0");
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";1");
			}
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]);
			}
			StartCoroutine(DeleteServantParticleSystem(servant, substractAttackPoints, GameParameter.allServants));
		}
	}
	
	public IEnumerator DeleteServantParticleSystem(GameObject servant, int substractAttackPoints, List<GameObject> servants) {
		GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
		psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[substractAttackPoints];
		psObject.particleSystem.Emit(1);
		yield return new WaitForSeconds (1.5f);
		psObject = servant.transform.Find("X").gameObject;
		psObject.particleSystem.Emit(1);
		yield return new WaitForSeconds (1f);
		servant.transform.position += new Vector3(1500f,0,0);
		servant.name = "tot";
		servants.Remove(servant);
		HighlightPlayableCard.setDienerPosition ();
		HighlightPlayableCard.setGegnerDienerPosition ();
	}
	
	[RPC]
	public void SetLife(string who, int newLife, int substractAttackPoints) {
		Debug.Log (newLife);
		string[] name = who.Split (';');
		if (name[0] == "Diener") {
			GameObject servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";1");
			if (servant == null) {
				servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";0");
			}			
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = attackPoints[newLife];
			string[] nameServant = servant.name.Split(';');
			GameObject psObject = servant.transform.Find("Zzz").gameObject;
			psObject.particleSystem.Play();
			psObject.SetActive(true);
			GameObject psObjectServant = servant.transform.Find("GotAttacked").gameObject;
			psObjectServant.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[substractAttackPoints];
			psObjectServant.particleSystem.Emit(1);
			servant.name = "DienerGegner" +";"+ newLife +";"+ name[2]+";"+nameServant[3];
		} else {
			GameObject servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";0");
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";1");
			}
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]);
			}
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = attackPoints[newLife];
			GameObject psObjectServant = servant.transform.Find("GotAttacked").gameObject;
			psObjectServant.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[substractAttackPoints];
			psObjectServant.particleSystem.Emit(1);
			servant.name = "Diener" +";"+ newLife +";"+ name[2]+";0";
		}
	}
	
	public static void setDienerPosition() {
		int countServants = GameParameter.allServants.Count;
		
		int add = 0;
		Vector3 pos;
		
		if (countServants % 2 != 0) {
			switch(countServants) {
			case 1: 
				add = 0 * 95;
				break;
			case 3: 
				add = 1 * 95;
				break;
			case 5: 
				add = 2 * 95;
				break;
			case 7: 
				add = 3 * 95;
				break;
			}
			countServants--;
			int i = GameParameter.allServants.Count() - 1;
			while (countServants >= 0) {
				pos = new Vector3(-201f+add,13f,431f);
				GameParameter.allServants[i].transform.position = pos;
				i--;
				countServants--;
				add -= 95;
			}
		} else {
			switch(countServants) {
			case 2: 
				add = 0 * 95;
				break;
			case 4: 
				add = 1 * 95;
				break;
			case 6: 
				add = 2 * 95;
				break;
			case 8: 
				add = 3 * 95;
				break;
			}
			countServants--;
			int i  = GameParameter.allServants.Count() - 1;
			while (countServants >= 0) {
				pos = new Vector3(-153.5f+add,13f,431f);
				GameParameter.allServants[i].transform.position = pos;
				i--;
				countServants--;
				add -= 95;
			}
		}
	}
	
	public static void setGegnerDienerPosition() {
		int countServants = GameParameter.allServantsOpponent.Count;
		int add = 0;
		Vector3 pos;
		
		if (countServants % 2 != 0) {
			switch(countServants) {
			case 1: 
				add = 0 * 95;
				break;
			case 3: 
				add = 1 * 95;
				break;
			case 5: 
				add = 2 * 95;
				break;
			case 7: 
				add = 3 * 95;
				break;
			}
			countServants--;
			int i = GameParameter.allServantsOpponent.Count()-1;
			while (countServants >= 0) {
				pos = new Vector3(-201f+add,13f,563f);
				GameParameter.allServantsOpponent[i].transform.position = pos;
				i--;
				countServants--;
				add -= 95;
			}
		} else {
			switch(countServants) {
			case 2: 
				add = 0 * 95;
				break;
			case 4: 
				add = 1 * 95;
				break;
			case 6: 
				add = 2 * 95;
				break;
			case 8: 
				add = 3 * 95;
				break;
			}
			countServants--;
			int i = GameParameter.allServantsOpponent.Count()-1;
			while (countServants >= 0) {
				pos = new Vector3(-153.5f+add,13f,563f);
				GameParameter.allServantsOpponent[i].transform.position = pos;
				i--;
				countServants--;
				add -= 95;
			}
		}
	}
}