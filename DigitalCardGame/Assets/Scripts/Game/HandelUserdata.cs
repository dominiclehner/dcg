﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class HandelUserdata : MonoBehaviour {

	public GameObject EnergyPanel;
	
	public Image e1;
	public Image e2;
	public Image e3;
	public Image e4;
	public Image e5;
	public Image e6;
	public Image e7;
	public Image e8;
	public Image e9;
	public Image e10;

	public Image[] energy;

	public Color initialColor;

	public Sprite basicSprite;
	public Sprite specialSprite1;
	public Sprite specialSprite2;
	public Sprite specialSprite3;
	public Sprite specialSprite4;

	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite4;

	public GameObject attackDistractionBeforeAnythingOtherPanel;
	public GameObject attackDistractionBeforeAnythingOtherBackground;
	public Text attackDistractionBeforeAnythingOtherText;

	// Use this for initialization
	void Start () {
		initialColor = e1.color;
		energy = new Image[] {e1, e2,e3,e4,e5,e6,e7,e8,e9,e10};
		GameParameter.energyCounter = 1;
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < 10; i++) {
			if (i < GameParameter.energyCounter) {
				energy[i].color = initialColor;
				energy[i].transform.Rotate(0.0f, 0.0f, -0.5f);
			} else {
				energy[i].color = new Color(1f,1f,1f,0.5f);
				energy[i].transform.Rotate(0.0f, 0.0f, -0.5f);
			}
		}
	}

	public void ClickedReady() {
		if (GameParameter.myTurn) {
			GameParameter.timer = 50f;
			GameParameter.myTurn = false;
			if (GameParameter.roundCounter < 10) {
				GameParameter.roundCounter++;
			}
			GameParameter.energyCounter = GameParameter.roundCounter;
			GameObject psObject;
			foreach (var item in GameParameter.allServantsOpponent) {
				item.transform.Find("Main").renderer.materials[1].color = Color.white;
				item.GetComponent<Servant>().isFirstRound = false;
				psObject = item.transform.Find("Zzz").gameObject;
				psObject.particleSystem.Stop();
				psObject.SetActive(false);
			}
			networkView.RPC ("ClickedReadyAndNotifyOpponentThatItIsHisTurn", RPCMode.Others, null);
			Timer.setNameOfServants();
		}
	}

	[RPC]
	public void ClickedReadyAndNotifyOpponentThatItIsHisTurn() {
		GameParameter.myTurn = true;
		GameParameter.timer = 50f;
		if (GameParameter.playableCards.Count () < 8) {
			int x = UnityEngine.Random.Range (0, Parameter.deckCards.Count);
			GameParameter.playableCards.Add (Parameter.deckCards [x]);
			Parameter.deckCards.RemoveAt (x);
			FillPlayableCardsUserData ();
		}
		StartCoroutine (activatePanel());
	}

	public IEnumerator  activatePanel(){
		attackDistractionBeforeAnythingOtherText.text = "Du bist am Zug!";
		attackDistractionBeforeAnythingOtherPanel.SetActive (true);
		attackDistractionBeforeAnythingOtherPanel.GetComponent<Animator>().Play("NotificationSlideIn");
		yield return new WaitForSeconds(3.2f);
		attackDistractionBeforeAnythingOtherPanel.SetActive (false);
	}

	public void FillPlayableCardsUserData() {
		for (int i = 0; i < GameParameter.playableCards.Count(); i++) {
			GameParameter.playableCardsGameObjects[i].SetActive(true);
		}
		int count = 0;
		foreach (var card in GameParameter.playableCards) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			string[] splitText = card.Text.Split(' ');
			if (splitText[0] == "Fügt" || splitText[0] == "Zieht" || splitText[0] == "Erhöht") {
				compCard [0].GetComponent<Text> ().text = "";
				compCard [1].GetComponent<Text> ().text = "";
			} else {
				compCard [0].GetComponent<Text> ().text = card.Leben.ToString();
				compCard [1].GetComponent<Text> ().text = card.Angriff.ToString();
			}
			compCard[2].GetComponent<Text>().text = card.Energy.ToString();
			compCard[3].GetComponent<Text>().text = card.Character.ToString();
			compCard[4].GetComponent<Text>().text = card.Id.ToString ();
			compCard[5].GetComponent<Text>().text = card.Text;
			if (card.Character == 0) {
				cardObject.GetComponent<SpriteRenderer>().sprite = basicSprite;
			} else if (card.Character == 1) {
				if (splitText[0] == "Erhöht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite1;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite1;
				}
			} else if (card.Character == 2) {
				if (splitText[0] == "Fügt") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite2;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite2;
				}
			} else if (card.Character == 3) {
				if (splitText[0] == "Zieht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite3;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite3;
				}
			} else if (card.Character == 4) {
				if (splitText[0] == "Erhöht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite4;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite4;
				}
			}
			cardObject.GetComponent<BoxCollider>().enabled = true;
			count++;
		}
		while (count < GameParameter.playableCardsGameObjects.Count()) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			//GameParameter.playableCardsGameObjects[count].SetActive(false);
			compCard[0].GetComponent<Text>().text = "";
			compCard[1].GetComponent<Text>().text = "";
			compCard[2].GetComponent<Text>().text = "";
			compCard[3].GetComponent<Text>().text = "";
			compCard[4].GetComponent<Text>().text = "";
			compCard[5].GetComponent<Text>().text = "";
			cardObject.GetComponent<SpriteRenderer>().sprite = null;
			cardObject.GetComponent<BoxCollider>().enabled = false;
			count++;
		}
		foreach (var c in GameParameter.playableCardsGameObjects) {
			string[] name = c.name.Split(';');
			c.name = name[0]+";0";
		}
	}

	public void ClickedFinish(){
		Network.Disconnect ();
		MasterServer.UnregisterHost ();
		Application.LoadLevel ("MainMenu");
	}
}
