﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Presentation : MonoBehaviour {

	int count;

	public Text topic;
	public GameObject StartScreen;
	public GameObject WhatisDCG;
	public GameObject InitialSituation;
	public GameObject Goals;
	public GameObject CurrentSituation;
	public GameObject Technologies;
	public GameObject LiveDemo;
	public GameObject TopicPanel;

	// Use this for initialization
	void Start () {
		count = -1;
		SetNextText();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			count++;
			SetNextText();
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			count--;
			SetNextText();
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.LoadLevel("Login");
		}
	}

	public void SetNextText () {
		switch (count) {
		case -1:
			TopicPanel.SetActive(false);
			StartScreen.SetActive(true);
			topic.text = "";
			WhatisDCG.SetActive(false);
			InitialSituation.SetActive(false);
			Goals.SetActive(false);
			CurrentSituation.SetActive(false);
			Technologies.SetActive(false);
			LiveDemo.SetActive(false);
			break;
		case 0:
			StartScreen.SetActive(false);
			TopicPanel.SetActive(true);
			topic.text = "What is dcg?";
			WhatisDCG.SetActive(true);
			InitialSituation.SetActive(false);
			Goals.SetActive(false);
			CurrentSituation.SetActive(false);
			Technologies.SetActive(false);
			LiveDemo.SetActive(false);
			break;
		case 1:
			StartScreen.SetActive(false);
			TopicPanel.SetActive(true);
			topic.text = "Initial Situation";
			WhatisDCG.SetActive(false);
			InitialSituation.SetActive(true);
			Goals.SetActive(false);
			CurrentSituation.SetActive(false);
			Technologies.SetActive(false);
			LiveDemo.SetActive(false);
			break;
		case 2:
			StartScreen.SetActive(false);
			TopicPanel.SetActive(true);
			topic.text = "Goals";
			WhatisDCG.SetActive(false);
			InitialSituation.SetActive(false);
			Goals.SetActive(true);
			CurrentSituation.SetActive(false);
			Technologies.SetActive(false);
			LiveDemo.SetActive(false);
			break;
		case 3:
			StartScreen.SetActive(false);
			TopicPanel.SetActive(true);
			topic.text = "Current Situation";
			WhatisDCG.SetActive(false);
			InitialSituation.SetActive(false);
			Goals.SetActive(false);
			CurrentSituation.SetActive(true);
			Technologies.SetActive(false);
			LiveDemo.SetActive(false);
			break;
		case 4:
			StartScreen.SetActive(false);
			TopicPanel.SetActive(true);
			topic.text = "Technologies";
			WhatisDCG.SetActive(false);
			InitialSituation.SetActive(false);
			Goals.SetActive(false);
			CurrentSituation.SetActive(false);
			Technologies.SetActive(true);
			LiveDemo.SetActive(false);
			break;
		case 5:
			StartScreen.SetActive(false);
			TopicPanel.SetActive(true);
			topic.text = "";
			WhatisDCG.SetActive(false);
			InitialSituation.SetActive(false);
			Goals.SetActive(false);
			CurrentSituation.SetActive(false);
			Technologies.SetActive(false);
			LiveDemo.SetActive(true);
			break;
		}
	}
}
