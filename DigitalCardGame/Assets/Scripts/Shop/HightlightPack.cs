﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HightlightPack : MonoBehaviour {

	Color initialColor;
	public Text text;
	public Text packname;
	public Text coins;
	public Text anzahl;
	public GameObject coinsImage;

	// Use this for initialization
	void Start () {
		initialColor = this.GetComponent<SpriteRenderer> ().color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter() {
		Debug.Log ("Enter");
		this.GetComponent<SpriteRenderer> ().color = Color.green;
		if (this.name == "Pack1") {
			packname.text = "Garantie Pack";
			text.text = "Zieht 5 Karten, eine dieser Karten hat mit Sicherheit mindestens 7 Energy oder mehr, und fügt sie zu euren Karten hinzü.";
			coins.text = "1500";
			anzahl.text = "Anzahl der Karten: 5";
			coinsImage.SetActive(true);
		} else {
			packname.text = "Standart Pack";
			text.text = "Zieht 5 Karten und fügt sie zu euren Karten hinzü.";
			coins.text = "1000";
			anzahl.text = "Anzahl der Karten: 5";
			coinsImage.SetActive(true);
		}

	}
	
	void OnMouseExit() {
		this.GetComponent<SpriteRenderer> ().color = initialColor;
		coinsImage.SetActive(false);
		packname.text = "Wähle ein Pack aus";
		text.text = "";
		coins.text = "";
		anzahl.text = "";
	}
}
