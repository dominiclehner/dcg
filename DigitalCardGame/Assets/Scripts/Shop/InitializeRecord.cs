﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InitializeRecord : MonoBehaviour {
	
	public Text usernameAndLevel;
	public Text levelPoints;
	public Text coins;
	public Text record;
	public Image level;
	
	// Use this for initialization
	void Start () {
		Parameter.coinsPanel = coins;
		CalculateLevel();
		usernameAndLevel.text = Parameter.Username + " - Level " + Parameter.Level;
		levelPoints.text = Parameter.Levelpoints + "/" + CalculateLevelpointsToReach();
		coins.text = "" + Parameter.Coins;
		record.text = "Bilanz: " + Parameter.GamesWon + " - " + Parameter.GamesLost;
		Parameter.Level -= 1;
		int test = CalculateLevelpointsToReach();
		Parameter.Level += 1;
		level.fillAmount = (float)(Parameter.Levelpoints - test) / (float)(CalculateLevelpointsToReach () - test);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	/// <summary>
	/// Calculates the levelpoints to reach.
	/// </summary>
	/// <returns>The levelpoints to reach.</returns>
	public int CalculateLevelpointsToReach () {
		int x = 100;
		if (Parameter.Level == 1) {
			x = 100;
		} else if (Parameter.Level == 2) {
			x = 200;
		} else if (Parameter.Level == 3) {
			x = 400;
		} else if (Parameter.Level == 4) {
			x = 800;
		} else if (Parameter.Level == 5) {
			x = 1600;
		} else if (Parameter.Level == 6) {
			x = 3200;
		} else if (Parameter.Level == 7) {
			x = 6400;
		} else if (Parameter.Level == 8) {
			x = 12800;
		} else if (Parameter.Level == 9) {
			x = 25600;
		} else if (Parameter.Level == 10) {
			x = 1000000;
		}
		return x;
	}
	
	/// <summary>
	/// Calculates the level.
	/// </summary>
	void CalculateLevel() {
		if (Parameter.Levelpoints < 100) {
			Parameter.Level = 1;
		} else if (Parameter.Levelpoints < 200) {
			Parameter.Level = 2;
		} else if (Parameter.Levelpoints < 400) {
			Parameter.Level = 3;
		} else if (Parameter.Levelpoints < 800) {
			Parameter.Level = 4;
		} else if (Parameter.Levelpoints < 1600) {
			Parameter.Level = 5;
		} else if (Parameter.Levelpoints < 3200) {
			Parameter.Level = 6;
		} else if (Parameter.Levelpoints < 6400) {
			Parameter.Level = 7;
		} else if (Parameter.Levelpoints < 12800) {
			Parameter.Level = 8;
		} else if (Parameter.Levelpoints < 25600) {
			Parameter.Level = 9;
		} else {
			Parameter.Level = 10;
		} 
	}
	
	public void ClickedBack() {
		Application.LoadLevel ("MainMenu");
	}
}
