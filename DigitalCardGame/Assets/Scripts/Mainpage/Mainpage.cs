﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Expressions;

public class Mainpage : MonoBehaviour {
	public Text usernameAndLevel;
	public Text levelPoints;
	public Text coins;
	public Text record;
	public Image level;
	public Image loadImage;
	public GameObject loadScreen;
	public GameObject userdata;
	public GameObject rewardPanel;
	public GameObject rewardPanelButton;
	public Color initialColor;
	public GameObject mainMenu;
	void Update() {
		loadImage.transform.Rotate(0.0f, 0.0f, -2.0f);
	}
	void Start() {
		try {
			//mainMenu.GetComponent<Animator> ().Play ("MenuSlideIn");
			initialColor = rewardPanelButton.GetComponent<Image> ().color;
			rewardPanelButton.GetComponent<Image> ().color = Color.grey;
			rewardPanelButton.GetComponent<Button> ().interactable = false;
			rewardPanel.SetActive (false);
			loadScreen.SetActive (true);
			userdata.SetActive (false);
			StartCoroutine(SendData ());
			List<Card> newCards = new List<Card> ();
			
			foreach (var card in Parameter.allCards.OrderBy(o => o.Id)) {
				foreach (var cardID in Parameter.myCards) {
					if (cardID == card.Id.ToString()) {
						newCards.Add(card);
					}
				}
			}
			Parameter.allCards = newCards.OrderBy(o => o.Energy).ToList();
				LoadAllCards.allCards = newCards.OrderBy(o => o.Energy).ToList();
		} catch (Exception ex) {
			ex.ToString();
			Application.LoadLevel("Login");
		}
	}
	public IEnumerator SendData() {
		string saveUrl = "http://digitalcardgame.bplaced.net/load.php";
		WWWForm form = new WWWForm ();
		form.AddField ("char", 0);
		WWW www = new WWW(saveUrl, form);
		yield return www;
		if (www.error != null) {
			print(www.error);
			Debug.Log(www.error);
		} else {
			SplitData(www.text);
		}
	}
	/// <summary>
	/// Splits the cards.
	/// </summary>
	/// <param name="text">Text.</param>
	public void SplitData(string text) {
		string[] line = text.Split ('\n');
		for (int i = 0; i < line.Length-1; i++) {
			string[] act = line [i].Split (';');
			if (Parameter.Username == act[1]) {
				Parameter.Coins = System.Int32.Parse(act[3]);
				Parameter.Level = System.Int32.Parse(act[4]);
				Parameter.Levelpoints = System.Int32.Parse(act[5]);
				Parameter.GamesWon = System.Int32.Parse(act[6]);
				Parameter.GamesLost = System.Int32.Parse(act[7]);
			}
		}
		CalculateLevel();
		usernameAndLevel.text = Parameter.Username + " - Level " + Parameter.Level;
		levelPoints.text = Parameter.Levelpoints + "/" + CalculateLevelpointsToReach();
		coins.text = "" + Parameter.Coins;
		record.text = "Bilanz: " + Parameter.GamesWon + " - " + Parameter.GamesLost;
		Parameter.Level -= 1;
		int test = CalculateLevelpointsToReach();
		Parameter.Level += 1;
		//Debug.Log ((float)((Parameter.Levelpoints-test) + "/" + (float)((CalculateLevelpointsToReach()-test) + " = " + ((float)((float)((Parameter.Levelpoints-test) / (float)((CalculateLevelpointsToReach()-test)));
		level.fillAmount = (float)(Parameter.Levelpoints-test) / (float)(CalculateLevelpointsToReach()-test);
		//level.fillAmount = (float) Parameter.Levelpoints / (float) CalculateLevelpointsToReach();
		loadScreen.SetActive (false);
		userdata.SetActive (true);
		if (Parameter.Levelpoints == 0) {
			//rewardPanel.SetActive(true);
			StartCoroutine(SendCoins (Parameter.Username, Parameter.Coins + 20, Parameter.Levelpoints + 10));
		}
	}
	public IEnumerator SendCoins(string username, int c, int poins) {
		string saveUrl = "http://digitalcardgame.bplaced.net/save_coins.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("coins", c);
		form.AddField ("poins", poins);
		form.AddField ("won", Parameter.GamesWon);
		form.AddField ("lost", Parameter.GamesLost);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
		} else {
			if (www.text != "False") {
				Debug.Log("Gespeichert");
				Parameter.Coins += 20;
				Parameter.Levelpoints += 10;
				CalculateLevel();
				usernameAndLevel.text = Parameter.Username + " - Level " + Parameter.Level;
				levelPoints.text = Parameter.Levelpoints + "/" + CalculateLevelpointsToReach();
				coins.text = "" + Parameter.Coins;
				record.text = "Bilanz: " + Parameter.GamesWon + " - " + Parameter.GamesLost;
				Parameter.Level -= 1;
				int test = CalculateLevelpointsToReach();
				Debug.Log("Letzte grenze: " + test);
				Parameter.Level += 1;
				level.fillAmount = (Parameter.Levelpoints-test) / (CalculateLevelpointsToReach());
				Debug.Log("diese grenze: " + CalculateLevelpointsToReach());
			}
			www.Dispose ();
		}
		rewardPanelButton.GetComponent<Image> ().color = initialColor;
		rewardPanelButton.GetComponent<Button> ().interactable = true;
	}
	void CalculateLevel() {
		if (Parameter.Levelpoints < 100) {
			Parameter.Level = 1;
		} else if (Parameter.Levelpoints < 200) {
			Parameter.Level = 2;
		} else if (Parameter.Levelpoints < 400) {
			Parameter.Level = 3;
		} else if (Parameter.Levelpoints < 800) {
			Parameter.Level = 4;
		} else if (Parameter.Levelpoints < 1600) {
			Parameter.Level = 5;
		} else if (Parameter.Levelpoints < 3200) {
			Parameter.Level = 6;
		} else if (Parameter.Levelpoints < 6400) {
			Parameter.Level = 7;
		} else if (Parameter.Levelpoints < 12800) {
			Parameter.Level = 8;
		} else if (Parameter.Levelpoints < 25600) {
			Parameter.Level = 9;
		} else {
			Parameter.Level = 10;
		} 
	}
	public int CalculateLevelpointsToReach () {
		int x = 0;
		if (Parameter.Level == 1) {
			x = 100;
		} else if (Parameter.Level == 2) {
			x = 200;
		} else if (Parameter.Level == 3) {
			x = 400;
		} else if (Parameter.Level == 4) {
			x = 800;
		} else if (Parameter.Level == 5) {
			x = 1600;
		} else if (Parameter.Level == 6) {
			x = 3200;
		} else if (Parameter.Level == 7) {
			x = 6400;
		} else if (Parameter.Level == 8) {
			x = 12800;
		} else if (Parameter.Level == 9) {
			x = 25600;
		} else if (Parameter.Level == 10) {
			x = 1000000;
		}
		return x;
	}
	public void ClickedBack () {
		Parameter.characterId = ""; 
		Parameter.Username = ""; 
		Parameter.deckCards = null; 
		Parameter.playableCards = null; 
		Parameter.gotDeck = false;
		Parameter.NewOrEditCardDeck = false;
		StartCoroutine (LoadNextScene("Login"));
	}
	public void CardDeckClicked () {
		StartCoroutine (LoadNextScene("CardDeckNewEdit"));
	}
	public void ClickedOptions () {
		StartCoroutine (LoadNextScene("Options"));
	}
	public void StartGame () {
		StartCoroutine (LoadNextScene("Game"));
	}
	public void ClickedAcceptReward() {
		rewardPanel.SetActive (false);
	}
	public void ClickedShop () {
		StartCoroutine (LoadNextScene("Shop"));
	}
	
	public IEnumerator LoadNextScene(string x) {
		yield return new WaitForSeconds (0.3f);
		Application.LoadLevel (x);
	}
}