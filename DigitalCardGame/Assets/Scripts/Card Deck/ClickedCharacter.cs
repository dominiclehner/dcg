﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ClickedCharacter : MonoBehaviour {

	private Vector3 originalScale;
	public GameObject character;
	public Color initialColor;

	public GameObject panelLinks;
	public GameObject panelRechts;
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter () {
		if (character.name == "Charackter_1") {
			panelLinks.SetActive(true);
			Text[] tL = panelLinks.GetComponentsInChildren<Text>();
			for (int i = 0; i < tL.Length; i++) {
				if (tL[i].name == "Name") {
					tL[i].text = "Hatroth";
				}
				if (tL[i].name == "StärkenText") {
					tL[i].text = "Hat viele Diener mit geringen Energykosten, die jedoch gut zu gebrauchen sind.";
				}
				if (tL[i].name == "SchwächenText") {
					tL[i].text = "Könnte sein das Ihnen die Karten ausgehen.";
				}
				if (tL[i].name == "SpezialfähigkeitText") {
					tL[i].text = "Stellt einen Diener mit 1/1 her. Kosten: 2 Energie";
				}
			}
		} else if (character.name == "Charackter_2") {
			panelRechts.SetActive(true);
			Text[] tR = panelRechts.GetComponentsInChildren<Text>();
			for (int i = 0; i < tR.Length; i++) {
				if (tR[i].name == "Name") {
					tR[i].text = "Astynia";
				}
				if (tR[i].name == "StärkenText") {
					tR[i].text = "Verursacht enorme Mengen an Schaden und Schmerzen.";
				}
				if (tR[i].name == "SchwächenText") {
					tR[i].text = "Kann sich selbst kaum heilen.";
				}
				if (tR[i].name == "SpezialfähigkeitText") {
					tR[i].text = "Fügt dem Geger 2 Schaden zu. Kosten: 2 Energie";
				}
			}
		} else if (character.name == "Charackter_3") {
			panelLinks.SetActive(true);
			Text[] tL = panelLinks.GetComponentsInChildren<Text>();
			for (int i = 0; i < tL.Length; i++) {
				if (tL[i].name == "Name") {
					tL[i].text = "Troogshy";
				}
				if (tL[i].name == "StärkenText") {
					tL[i].text = "Bekommt mehr Karten auf seine Hand.";
				}
				if (tL[i].name == "SchwächenText") {
					tL[i].text = "Diener haben oft hohe Energykosten.";
				}
				if (tL[i].name == "SpezialfähigkeitText") {
					tL[i].text = "Zieht eine Karte aus seinen Deck. Kosten: 2 Energie";
				}
			}
		} else if (character.name == "Charackter_4") {
			panelRechts.SetActive(true);
			Text[] tR = panelRechts.GetComponentsInChildren<Text>();
			for (int i = 0; i < tR.Length; i++) {
				if (tR[i].name == "Name") {
					tR[i].text = "Atona";
				}
				if (tR[i].name == "StärkenText") {
					tR[i].text = "Verfügt über mächtige Heilungen und Stärkungszaubern.";
				}
				if (tR[i].name == "SchwächenText") {
					tR[i].text = "Verursachter Schaden seiner Diener hält sich in Grenzen.";
				}
				if (tR[i].name == "SpezialfähigkeitText") {
					tR[i].text = "Heilt sich selbst um 2 Lebenspunkte. Kosten: 2 Energie";
				}
			}
		}
		originalScale = transform.localScale ;
		transform.localScale = new Vector3(150,150,150);
		initialColor = character.renderer.material.color;
		character.renderer.material.color = Color.green;

	}

	void OnMouseExit () {
		panelLinks.SetActive (false);
		panelRechts.SetActive (false);
		transform.localScale = originalScale;
		character.renderer.material.color = initialColor;
	}

	void OnMouseDown () {
		Parameter.characterId = this.GetComponentInChildren<Text>().text;
		Application.LoadLevel ("CardSelection");
	}
}
